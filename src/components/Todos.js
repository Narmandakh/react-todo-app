import {
    useState,
    useEffect
} from 'react'
import firebase from './../plugins/firebase'

const Todos = props => {
    const [
        todo,
        setTodo
    ] = useState('')
    const [
        todos,
        setTodos
    ] = useState([])

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = () => {
        firebase.ref('todos').on('value', (snapshot) => {
            setTodos(Object.values(snapshot.val()))
        })
    }

    const handleChange = e => {
        setTodo(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()

        firebase.ref('todos').push(todo)

        setTodo('')
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input
                    placeholder="Enter your todo bit...."
                    value={todo}
                    onChange={handleChange}
                />
                <button>Add</button>
            </form>
            <ul>
                {todos.map((todo, index) =>
                    <li key={index}>{todo}</li>
                )}
            </ul>
        </div>
    )
}

export default Todos