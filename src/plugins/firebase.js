import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const firebaseConfig = {
    apiKey: "AIzaSyBzNVZa7rb-1cWxuWj2_6Ll9pwcAAMSs1U",
    authDomain: "shits-ca25f.firebaseapp.com",
    databaseURL: "https://shits-ca25f.firebaseio.com",
    projectId: "shits-ca25f",
    storageBucket: "shits-ca25f.appspot.com",
    messagingSenderId: "886663792007",
    appId: "1:886663792007:web:b57d4dac26acb5bd943f22",
    measurementId: "G-VFSJBBP86S"
}

firebase.initializeApp(firebaseConfig)

var database = firebase.database()

export default database